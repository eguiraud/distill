#pragma once
#include <doctest/doctest.h>
#include <util.hpp>

#include "test_util.hpp" // STATIC_CHECK

TEST_CASE("util tuple_split") {
  constexpr auto t = std::make_tuple(1, 2., 3.f);
  constexpr auto s0 = tuple_split<0>(t);
  static_assert(std::tuple_size<decltype(s0.first)>::value == 0);
  static_assert(std::tuple_size<decltype(s0.second)>::value == 3);
  STATIC_CHECK(s0.second == std::make_tuple(1, 2., 3.f));

  constexpr auto s1 = tuple_split<1>(t);
  static_assert(std::tuple_size<decltype(s1.first)>::value == 1);
  static_assert(std::tuple_size<decltype(s1.second)>::value == 2);
  STATIC_CHECK(s1.first == std::make_tuple(1));
  STATIC_CHECK(s1.second == std::make_tuple(2., 3.f));
}
