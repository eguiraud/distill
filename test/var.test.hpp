#pragma once
#include <doctest/doctest.h>
#include <cstring>
#include <var.hpp>

constexpr bool ConstexprStrEq(const char *s1, const char *s2) {
  std::size_t c = 0;
  for (; s1[c] != '\0' && s2[c] != '\0'; ++c) {
    if (s1[c] != s2[c])
      return false;
  }
  return (s1[c] == '\0' && s2[c] == '\0');
}

TEST_CASE("Var ctor") {
  constexpr auto x = Var<int>("x");
  constexpr auto s = x.name();
  static_assert(ConstexprStrEq(s, "x"));
  CHECK_EQ(std::strcmp(x.name(), "x"), 0); // gcov sees this invocation
}
