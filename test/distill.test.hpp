#pragma once
#include <distill.hpp>
#include <doctest/doctest.h>

#include "test_util.hpp" // STATIC_CHECK

TEST_CASE("distill Var") {
  Var<int> x("x");
  auto xx = distill(x);
  STATIC_CHECK(xx(0) == 0);
}

TEST_CASE("distill sum") {
  constexpr auto x = Var<int>("x");
  constexpr auto y = Var<int>("y");
  constexpr auto sum = Sum(x, y);
  constexpr auto s = distill(sum);
  STATIC_CHECK(s(1, 2) == 3);
}

TEST_CASE("distill sum of sum") {
  constexpr Var<int> x("x"), y("y");
  constexpr Var<float> z("z");
  constexpr auto sum = Sum(Sum(x, y), z);
  constexpr auto s = distill(sum);
  STATIC_CHECK(s(1, 2, 3) == 6);
}

TEST_CASE("distill plus") {
  constexpr Var<int> x("x");
  constexpr Var<int> y("y");
  STATIC_CHECK(distill(x + y)(1, 2) == 3);

  constexpr Var<double> z("z");
  constexpr auto sumxy = x + y;
  constexpr auto sum_f = distill(sumxy + z);
  STATIC_CHECK(sum_f(1, 2, 3.) == 6);
}

TEST_CASE("distill binary ops") {
  constexpr Var<int> x("x"), y("y");
  constexpr auto expr = (x + y) / (x - y);
  STATIC_CHECK(distill(expr)(1, 1, 2, 1) == 2);
  constexpr auto expr2 = x * y / x * y;
  STATIC_CHECK(distill(expr2)(4, 4, 2, 2) == 16);
}

TEST_CASE("distill logical ops") {
  constexpr Var<bool> x("x");
  constexpr Var<bool> y("y");
  constexpr auto f = distill((x && y) || (x || y));
  STATIC_CHECK(f(true, true, true, false) == true);
}

TEST_CASE("distill math ops") {
  constexpr Var<double> x("x");
  constexpr Var<double> y("y");
  constexpr auto f = distill(x > y || x < y);
  STATIC_CHECK(f(1., 2., 1., 2.) == true);
  constexpr auto f2 = distill(x == y || x != y);
  STATIC_CHECK(f2(1., 2., 1., 2.) == true);
}
