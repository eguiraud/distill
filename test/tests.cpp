#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest/doctest.h>

#include "distill.test.hpp"
#include "typelist.test.hpp"
#include "util.test.hpp"
#include "var.test.hpp"
