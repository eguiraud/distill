#pragma once
#include <doctest/doctest.h>

// perform a static_assert as well as a runtime CHECK on the argument
#define STATIC_CHECK(x)                                                        \
  static_assert(x);                                                            \
  CHECK(x);
