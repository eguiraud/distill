#pragma once
#include <doctest/doctest.h>
#include <typelist.hpp>

TEST_CASE("typelist take") {
  TypeList<int, double> l;
  static_assert(Take<0>(l) == TypeList<>{});
  static_assert(Take<1>(l) == TypeList<int>{});
  static_assert(Take<2>(l) == TypeList<int, double>{});
}

TEST_CASE("typelist split") {
  TypeList<int, float, double> l;
  constexpr auto s0 = Split<0>(l);
  static_assert(s0.first == TypeList<>{});
  static_assert(s0.second == l);
  constexpr auto s1 = Split<1>(l);
  static_assert(s1.first == TypeList<int>{});
  static_assert(s1.second == TypeList<float, double>{});
  constexpr auto s2 = Split<2>(l);
  static_assert(s2.first == TypeList<int, float>{});
  static_assert(s2.second == TypeList<double>{});
  constexpr auto s3 = Split<3>(TypeList<int, float, double, void>{});
  static_assert(s3.first == TypeList<int, float, double>{});
  static_assert(s3.second == TypeList<void>{});
}
