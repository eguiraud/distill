# distill

`distill` is an elegant way to assemble efficient constexpr numerical functions.

1. perform operations on named variables, as if they were values: implicitly, this builds a compile-time computation graph
2. _distill_ constexpr-enabled callables from results, flattening the computation graph to a single operation

### Building a constexpr sum function
`distill`'s hello world program:
```c++
constexpr Var<int> x("x"), y("y");
constexpr auto sum_f = distill(x + y);
static_assert(sum_f(40, 2) == 42);
```
Removing the `constexpr` keyword will make the `static_assert` complain that `sum_f` is not a constant expression.<br>
`sum_f` will still be usable in any context that does not require a constant expression though:<br>
all distilled callables can be used as standard runtime functions.

### Constexpr function factory
Using a function factory is a simple way to encapsulate the distillation of a callable,<br>
without the noise of having to specify `constexpr` at every line.
```c++
constexpr auto make_sum() {
    Var<int> x("x"), y("y");
    return distill(x + y);
}

void test() { static_assert(make_sum()(40, 2) == 42); }
```