#pragma once
#include "binaryop.hpp"
#include "typelist.hpp"
#include "util.hpp" // tuple_split, get_t, decl
#include "var.hpp"

#include <tuple>       // std::apply
#include <type_traits> // std::decay_t
#include <utility>     // std::forward

template <class... Args>
constexpr auto ArgTypes(Args... args) {
  return Cat(ArgTypes(args)...);
}

template <template <class, class> class Op, class X, class Y>
constexpr auto ArgTypes(Op<X, Y> op) {
  return Cat(ArgTypes(op.first), ArgTypes(op.second));
}

template <class T>
constexpr auto ArgTypes(Var<T>) {
  return TypeList<T>{};
}

template <class Op, class ArgTypes>
struct Distilled {
  static_assert(sizeof(Op) < 0, "should never be instantiated");
};

template <class T>
struct Distilled<Var<T>, TypeList<T>> {
  constexpr Distilled(Var<T>, TypeList<T>) {}
  constexpr auto operator()(T t) const { return t; }
};

template <class T>
Distilled(Var<T>, TypeList<T>) -> Distilled<Var<T>, TypeList<T>>;

template <template <class, class> class BinOp, class Op1, class Op2,
          class... Args>
struct Distilled<BinOp<Op1, Op2>, TypeList<Args...>> {
  BinOp<Op1, Op2> _op;
  constexpr Distilled(BinOp<Op1, Op2> op, TypeList<Args...>) : _op(op) {}
  constexpr auto operator()(Args... args) const {
    constexpr auto n_args_op1 = decltype(ArgTypes(_op.first))::size;
    constexpr auto split_args = Split<n_args_op1>(TypeList<Args...>{});
    using TL2 = std::decay_t<decltype(split_args.second)>;
    using TL1 = std::decay_t<decltype(split_args.first)>;
    auto [first_args, second_args] = tuple_split<n_args_op1>(std::tie(args...));
    return _op.op(std::apply(Distilled<Op1, TL1>(_op.first, split_args.first),
                             first_args),
                  std::apply(Distilled<Op2, TL2>(_op.second, split_args.second),
                             second_args));
  }
};

template <template <class, class> class BinOp, class Op1, class Op2,
          class... ArgTypes>
Distilled(BinOp<Op1, Op2>, TypeList<ArgTypes...>)
    -> Distilled<BinOp<Op1, Op2>, TypeList<ArgTypes...>>;

template <class Node>
constexpr auto distill(Node n) {
  auto arg_types = ArgTypes(n);
  return Distilled(n, arg_types);
}
