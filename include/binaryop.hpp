#pragma once
#include "var.hpp"

#include <type_traits> // std::enable_if_t, std::decay_t, etc.
#include <utility>     // std::forward

template <class T>
constexpr inline bool IsDecayed = std::is_same_v<std::decay_t<T>, T>;

template <class NodeLeft, class NodeRight>
struct BinaryOp {
  static_assert(IsDecayed<NodeLeft> && IsDecayed<NodeRight>);

  NodeLeft first;
  NodeRight second;

  constexpr BinaryOp(NodeLeft nl, NodeRight nr) : first(nl), second(nr) {}
};

// Compile-time check that std::decay_t<T> is one of Var<T> or something that
// inherits from  BinaryOp
template <class T>
struct _IsDistillable : std::false_type {};

template <class NodeLeft, class NodeRight, template <class, class> class Op>
struct _IsDistillable<Op<NodeLeft, NodeRight>>
    : std::conditional_t<std::is_base_of_v<BinaryOp<NodeLeft, NodeRight>,
                                           Op<NodeLeft, NodeRight>>,
                         std::true_type, std::false_type> {};

template <class T>
struct _IsDistillable<Var<T>> : std::true_type {};

template <class T>
constexpr inline bool IsDistillable = _IsDistillable<std::decay_t<T>>::value;

// TODO instead of N subtypes, we could just use lambdas to differentiate the
// operators (also simplifies IsDistillable)
// TODO the input types of `op` could be known at compile time, in principle
#define BINARY_OP(NAME, OP)                                                    \
  template <class T, class U>                                                  \
  class NAME : public BinaryOp<T, U> {                                         \
  public:                                                                      \
    constexpr NAME(T t, U u) : BinaryOp<T, U>(t, u) {}                         \
                                                                               \
    template <class X, class Y>                                                \
    constexpr auto op(X &&x, Y &&y) const {                                    \
      return x OP y;                                                           \
    }                                                                          \
  };                                                                           \
                                                                               \
  template <class Node1, class Node2,                                          \
            typename = std::enable_if_t<IsDistillable<Node1> &&                \
                                        IsDistillable<Node2>>>                 \
  constexpr auto operator OP(Node1 &&n1, Node2 &&n2) {                         \
    return NAME<std::decay_t<Node1>, std::decay_t<Node2>>(                     \
        std::forward<Node1>(n1), std::forward<Node2>(n2));                     \
  }

BINARY_OP(Sum, +)
BINARY_OP(Subtract, -)
BINARY_OP(Multiply, *)
BINARY_OP(Divide, /)
BINARY_OP(Greater, >)
BINARY_OP(Less, <)
BINARY_OP(Equal, ==)
BINARY_OP(NotEqual, !=)
BINARY_OP(LogicalAnd, &&)
BINARY_OP(LogicalOr, ||)
BINARY_OP(BitwiseAnd, &)
BINARY_OP(BitwiseOr, |)
BINARY_OP(BitwiseXor, ^)
