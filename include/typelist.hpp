#pragma once
#include <cstddef>

template <class... Types>
struct TypeList {
  constexpr TypeList() = default;
  constexpr static auto size = sizeof...(Types);
};

template <class... Types>
constexpr bool operator==(TypeList<Types...>, TypeList<Types...>) {
  return true;
}

template <class... Types1, class... Types2>
constexpr bool operator==(TypeList<Types1...>, TypeList<Types2...>) {
  return false;
}

template <class... Types>
constexpr bool operator!=(TypeList<Types...> t1, TypeList<Types...> t2) {
  return !(t1 == t2);
}

template <class... Types1, class... Types2>
constexpr bool operator!=(TypeList<Types1...> t1, TypeList<Types2...> t2) {
  return !(t1 == t2);
}

template <class... Arg1, class... Arg2>
constexpr auto Cat(TypeList<Arg1...>, TypeList<Arg2...>) {
  return TypeList<Arg1..., Arg2...>{};
}

template <std::size_t N, class T, class... Types>
constexpr auto Take(TypeList<T, Types...>) {
  if constexpr (N == 0)
    return TypeList<>{};
  else if constexpr (N == 1)
    return TypeList<T>{};
  else
    return Cat(TypeList<T>{}, Take<N - 1>(TypeList<Types...>{}));
}

template <class List1, class List2>
struct SplitList {
  constexpr SplitList(List1, List2) {}
  constexpr static auto first = List1{};
  constexpr static auto second = List2{};
};

template <std::size_t N, class T1, class... Types1, class... Types2>
constexpr auto SplitHelper(TypeList<T1, Types1...> /*tosplit*/,
                           TypeList<Types2...> accumulator) {
  if constexpr (N == 1)
    return SplitList(Cat(accumulator, TypeList<T1>{}), TypeList<Types1...>{});
  else
    return SplitHelper<N - 1>(TypeList<Types1...>{},
                              Cat(accumulator, TypeList<T1>{}));
}

template <std::size_t N, class T, class... Types>
constexpr auto Split(TypeList<T, Types...>) {
  if constexpr (N == 0)
    return SplitList(TypeList<>{}, TypeList<T, Types...>{});
  else if constexpr (N == 1)
    return SplitList(TypeList<T>{}, TypeList<Types...>{});
  else
    return SplitHelper<N - 1>(TypeList<Types...>{}, TypeList<T>{});
}
