#pragma once
#include <cstddef>
#include <tuple>
#include <utility>

template <class T>
using get_t = typename T::type;

template <class T>
T &&decl();

template <bool B, class T>
constexpr auto make_tuple_if(T t) {
  if constexpr (B)
    return std::make_tuple(t);
  else
    return std::tuple<>{};
}

template <std::size_t N, std::size_t... S, class... Ts>
constexpr auto split_helper(std::index_sequence<S...>,
                            const std::tuple<Ts...> &t) {
  auto first = std::tuple_cat(make_tuple_if < S<N>(std::get<S>(t))...);
  auto second = std::tuple_cat(make_tuple_if<S >= N>(std::get<S>(t))...);
  return std::make_pair(first, second);
}

template <std::size_t N, class T>
constexpr auto tuple_split(const T &tuple) {
  using tuple_idxs = std::make_index_sequence<std::tuple_size<T>::value>;
  return split_helper<N>(tuple_idxs{}, tuple);
}
