#pragma once
#include <cstddef>

template <class T>
class Var {
  const char *_name;
  std::size_t _size;

public:
  template <std::size_t N>
  explicit constexpr Var(const char (&name)[N]) : _name(name), _size(N) {}
  constexpr const char *name() const { return _name; }

  using type = T;
};
