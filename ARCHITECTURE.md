# Architecture of the Distill library

## Overview

`Var<T>` is a placeholder for a named, typed value, e.g. `Var<int> x`.
`BinaryOp<Left,Right>` is a type that represents an operation between two operands;
it stores the operands as well as what operation to perform on them.
The operands can be of type `Var` or `BinaryOp` (in the future, maybe we can also allow
types for which `T` in `Var<T>` or the result type of a `BinaryOp` knows how to perform
the operation).

C++ binary operators are overloaded to act on `Var`s and `BinaryOp`s.
They return `BinaryOp` objects.

Calling `distill` on a `Var` or a `BinaryOp` returns a callable object of type `Distilled`,
which can be invoked passing as many values as the `Var`s that are present in the computation
graph, in the order in which they were added to it. It returns the result of the computation
performed by the computation graph with the values passed to the `Distilled::operator()` taking
the place of the `Var` objects (see the README for example usages that should make the
programming model clear).

Since `Var`s are lightweight objects, we can copy them around avoiding pesky dangling reference
problems that are common in expression templates/computation graph implementations.
`Var`s don't have an identity other than their name, so two different `Var` instances with the
same name will be treated as if they were the same variable by the computation graph.
(Currently we don't actually coalesce multiple usages of the same variable, so if you do
`Var<int> x("x"); auto fn = distill(x + x)` you actually have to pass 2 numbers into `fn`.
I hope to fix this in the future).

## How is the "distilling" done?

Starting from the root of the computation graph, we first build a flattened list of all the
variable types. (in the future: only listing each variable's type once even if a variable is
used multiple times in the computation graph).

Then we instantiate a `Distilled` object. The interesting logic is in `Distilled::operator()`,
which recursively instantiates and invokes other `Distilled` objects as needed, splitting the
values that were passed as input between the nested `Distilled` objects as needed.


